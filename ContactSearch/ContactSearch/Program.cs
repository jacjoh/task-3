﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace ContactSearch
{
    class Program
    {
        static void Main(string[] args)
        {
            List<string> contacts = new List<string>() { 
                "Jon Andreasen", 
                "Arne Bjørk", 
                "Lise Lotte",
                "Bjørn Bjørnsen",
                "Gerald Clark"
            };

            Console.WriteLine("This is a console based contacts app");
            Console.WriteLine("To search through the contacts type \"s\" and then the contact to search for");
            Console.WriteLine("To add a new contact type \"a\" and then the full name of the contact");
            Console.WriteLine("To remove a contact \"r\" and then the full name of the contact ");
            Console.WriteLine("To exit the program type e");

            string choice = "";

            while(choice != "e")
            {
                string userResponse = Console.ReadLine();

                //the first part of the string is the command, and the rest is the name 
                choice = userResponse.Split(" ")[0];
                string name = userResponse.Substring(userResponse.IndexOf(" ") + 1);
                
                switch (choice)
                {
                    case "s":
                        SearchContacts(contacts, name);
                        break;
                    case "a":
                        AddContact(contacts, name);
                        break;
                    case "r":
                        RemoveContact(contacts, name);
                        break;
                    default:
                        Console.WriteLine("To search through the contacts type \"s\" and then the contact to search for");
                        Console.WriteLine("To add a new contact type \"a\" and then the full name of the contact");
                        Console.WriteLine("To remove a contact \"r\" and then the full name of the contact ");
                        Console.WriteLine("To exit the program type e");
                        break;
                }
            }
        }

        static void RemoveContact(List<string> contacts, string name)
        {
            //must check if the name exists before removing it
            if (!contacts.Contains(name))
            {
                Console.WriteLine("There is no such name in your contacts");
            }

            contacts.Remove(name);
            Console.WriteLine($"{name} was successfully removed from your list of contacts");
        }

        static void AddContact(List<string> contacts, string name)
        {
            //should not add a element if there is no name to add
            if(name.Length == 0)
            {
                Console.WriteLine("You need to write a name to add");
                return;
            }

            contacts.Add(name);
        }

        static void SearchContacts(List<string> contacts, string searchString)
        {
            List<string> fullMatches = new List<string>();
            List<string> partialMatches = new List<string>();
            
            //convert the string to lowercase so that it does not affect the search
            string searchStringLower = searchString.ToLower();

            //Loop through the contacts and check for full or partial match
            foreach (string contact in contacts)
            {
                string contactLower = contact.ToLower();
                if (contactLower == searchStringLower)
                {
                    fullMatches.Add(contact);
                }
                //It is a partial match if the contact contains the search string
                if (contactLower.Contains(searchStringLower))
                {
                    partialMatches.Add(contact);
                }
            }

            //when the list has no elements there weren't any results
            if (!fullMatches.Any() && !partialMatches.Any())
            {
                Console.WriteLine("There were no results for your search.");
                return;
            }

            //Print full matches if there are any
            if (fullMatches.Any())
            {
                Console.WriteLine("Full match: ");
                foreach (string contact in fullMatches)
                {
                    Console.WriteLine("-" + contact);
                }
            } 
            
            Console.WriteLine("Partial matches: ");
            foreach (string contact in partialMatches)
            {
                Console.WriteLine("-" + contact);
            }
            
        }
    }
}

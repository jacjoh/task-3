# task-3

Console based application for searching though contacts.

The app is written in c#.

In the app there is possible to search for a contact, and the console will then display full matches and partial matches.
To search for a person in the app you type "s" and then the name to search for.
To add a new person in the app you type "a" and then the name to add.
To remove a person from the contacts you type "r" and then the name to remove.

